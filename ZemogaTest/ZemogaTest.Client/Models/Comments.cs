﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTest.Client.Models
{
    public class Comments
    {
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime PublishAt { get; set; }
    }
}
