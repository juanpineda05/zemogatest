﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTest.Client.Models
{
    public class CreatePost
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
    }
}
