﻿using System;
using System.Collections.Generic;

namespace ZemogaTest.Client.Models
{
    public class BlogPost
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public string State { get; set; }
        public DateTime? ApprovalAt { get; set; }
        public DateTime PublishAt { get; set; }
        public List<Comments> Comments { get; set; }
    }
}
