﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTest.Client.Models
{
    public class UserDomain
    {
        public string UserName { get; set; }

        public string Role { get; set; }
    }
}
