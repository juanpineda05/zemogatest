﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZemogaTest.Client.Models;

namespace ZemogaTest.Client.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            List<BlogPost> blogPostList = new List<BlogPost>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://zemogatestwebapi20191123114254.azurewebsites.net/api/Blog"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    blogPostList = JsonConvert.DeserializeObject<List<BlogPost>>(apiResponse);
                }
            }
            string roleName = HttpContext.Session.GetString("Role");
            ViewBag.ShowDelete = (!string.IsNullOrEmpty(roleName) && roleName == "Editor");
            return View(blogPostList);
        }

        public IActionResult About(string Id)
        {
            ViewBag.IdBlog = Id;
            ViewBag.SetUser = string.IsNullOrEmpty(HttpContext.Session.GetString("Role"));

            return View();
        }

        public IActionResult Contact()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("UserName")) || string.IsNullOrEmpty(HttpContext.Session.GetString("Role")))
            {
                return View();
            }
            else
            {
                return Redirect("~/Home");
            }


        }

        public IActionResult Privacy()
        {
            HttpContext.Session.Remove("UserName");
            HttpContext.Session.Remove("Role");
            return Redirect("~/Home/Contact");
        }

        public IActionResult CreatePost()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> SaveComment(Comments comments, string Id)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, "https://zemogatestwebapi20191123114254.azurewebsites.net/api/Comments"))
            {
                var json = JsonConvert.SerializeObject(new
                {
                    BlogId = Id,
                    UserName = string.IsNullOrEmpty(HttpContext.Session.GetString("UserName")) ? comments.UserName : HttpContext.Session.GetString("UserName"),
                    Text = comments.Text
                });
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    using (var response = await client.SendAsync(request))
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
            }
            return Redirect("~/Home");
        }

        [HttpPost]
        public async Task<IActionResult> Login(Login login)
        {

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, "https://zemogatestwebapi20191123114254.azurewebsites.net/api/User"))
            {
                var json = JsonConvert.SerializeObject(login);
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    using (var response = await client.SendAsync(request))
                    {
                        using (HttpContent content = response.Content)
                        {
                            var apiResponse = content.ReadAsStringAsync().Result;
                            UserDomain user = JsonConvert.DeserializeObject<UserDomain>(apiResponse);
                            if (user == null)
                            {
                                return Redirect("~/Home/Contact");
                            }
                            HttpContext.Session.SetString("UserName", user.UserName);
                            HttpContext.Session.SetString("Role", user.Role);
                            if (user.Role == "Writer")
                            {
                                return Redirect("~/Home/CreatePost");
                            }
                        }
                    }
                }
            }
            return Redirect("~/Home");
        }

        public async Task<IActionResult> DeletePost(string Id)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Delete, string.Format("https://zemogatestwebapi20191123114254.azurewebsites.net/api/Blog/{0}", Id)))
            {
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                }
            }
            return Redirect("~/Home");
        }

        [HttpPost]
        public async Task<IActionResult> SavePost(CreatePost createPost)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, "https://zemogatestwebapi20191123114254.azurewebsites.net/api/Blog"))
            {
                var json = JsonConvert.SerializeObject(new
                {
                    Id = createPost.Id,
                    Author = HttpContext.Session.GetString("UserName"),
                    Text = createPost.Text
                });
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    using (var response = await client.SendAsync(request))
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
            }


            return Redirect("~/Home");
        }

    }
}
