﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZemogaTest.DataAccess.DTO;
using ZemogaTest.WebApi.Models;

namespace ZemogaTest.WebApi.Helper
{
    public static class AutoMapperConfig
    {
        public static BlogDTO ConvertToDTO(BlogPost blogPost)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BlogPost, BlogDTO>();
            });

            IMapper iMapper = config.CreateMapper();
            return iMapper.Map<BlogDTO>(blogPost);
        }

        public static CommentsDTO ConvertToDTO(AddComment comment)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AddComment, CommentsDTO>();
            });

            IMapper iMapper = config.CreateMapper();
            return iMapper.Map<CommentsDTO>(comment);
        }
    }
}
