﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZemogaTest.DataAccess.DataAccess.Interfaces;
using ZemogaTest.DataAccess.DTO;
using ZemogaTest.WebApi.Helper;
using ZemogaTest.WebApi.Models;

namespace ZemogaTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly IBlogRepository _blogRepository;
        public CommentsController(IBlogRepository blogRepository)
        {
            this._blogRepository = blogRepository;
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<ActionResult> AddComments([FromBody]AddComment body)
        {
            try
            {
                CommentsDTO commentsDTO = AutoMapperConfig.ConvertToDTO(body);
                await _blogRepository.CreateComment(body.BlogId, commentsDTO);
                return StatusCode((int)HttpStatusCode.OK, new
                {
                    StatusCode = StatusCodes.Status200OK,
                    Message = "Comment has been added"
                });
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = string.Format("Error: {0}", ex)
                });
            }
        }
    }
}