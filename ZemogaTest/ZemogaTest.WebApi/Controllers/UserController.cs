﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZemogaTest.DataAccess.DataAccess.Interfaces;
using ZemogaTest.WebApi.Models;

namespace ZemogaTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        public UserController(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        // POST: api/User
        [HttpPost]
        public async Task<ActionResult<bool>> Post([FromBody]ValidateUser body)
        {
            return Ok(await _userRepository.ValidateUser(body.UserName, body.Password));
        }

    }
}
