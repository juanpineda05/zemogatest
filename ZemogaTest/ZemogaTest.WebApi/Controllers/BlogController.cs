﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZemogaTest.DataAccess.DataAccess.Interfaces;
using ZemogaTest.DataAccess.DTO;
using ZemogaTest.WebApi.Helper;
using ZemogaTest.WebApi.Models;

namespace ZemogaTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogRepository _blogRepository;
        public BlogController(IBlogRepository blogRepository)
        {
            this._blogRepository = blogRepository;
        }

        // GET: api/Blog
        [HttpGet]
        public async Task<IEnumerable<BlogDTO>> Get()
        {
            return await _blogRepository.Get(c => c.State == "Approve");
        }

        // POST: api/Blog
        [HttpPost]
        [Produces("application/json")]
        public async Task<ActionResult> Post([FromBody]BlogPost body)
        {
            try
            {
                BlogDTO blogDTO = AutoMapperConfig.ConvertToDTO(body);
                await _blogRepository.Create(blogDTO);
                return StatusCode((int)HttpStatusCode.OK, new
                {
                    StatusCode = StatusCodes.Status200OK,
                    Message = "BlogPost has been created"
                });
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = string.Format("Error: {0}", ex)
                });
            }
        }

        // POST: api/Blog/UpdateState
        [HttpPost]
        [Route("UpdateState")]
        [Produces("application/json")]
        public async Task<ActionResult> UpdateState([FromBody]UpdateStatePost body)
        {
            try
            {
                await _blogRepository.ChangeStatePost(body.Id, body.State);
                return StatusCode((int)HttpStatusCode.OK, new
                {
                    StatusCode = StatusCodes.Status200OK,
                    Message = string.Format("BlogPost with id: {0} has been approve/reject", body.Id)
                });
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = string.Format("Error: {0}", ex)
                });
            }
        }

        // PUT: api/Blog/5
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<ActionResult> Put(string id, [FromBody]BlogPost body)
        {
            try
            {
                BlogDTO blogDTO = AutoMapperConfig.ConvertToDTO(body);
                await _blogRepository.Update(id, blogDTO);
                return StatusCode((int)HttpStatusCode.OK, new
                {
                    StatusCode = StatusCodes.Status200OK,
                    Message = string.Format("BlogPost with id: {0} has been edited", id)
                });

            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = string.Format("Error: {0}", ex)
                });
            }

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                await _blogRepository.Delete(id);
                return StatusCode((int)HttpStatusCode.OK, new
                {
                    StatusCode = StatusCodes.Status200OK,
                    Message = string.Format("BlogPost with id: {0} has been deleted", id)
                });

            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    Message = string.Format("Error: {0}", ex)
                });
            }
        }

        [HttpGet]
        [Route("GetPendingApproval")]
        [Produces("application/json")]
        public async Task<IEnumerable<BlogDTO>> GetPendingApproval()
        {
            return await _blogRepository.Get(c => c.State == "Pending publish approval");
        }


    }
}
