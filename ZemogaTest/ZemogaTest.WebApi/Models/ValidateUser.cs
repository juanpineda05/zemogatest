﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTest.WebApi.Models
{
    public class ValidateUser
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
