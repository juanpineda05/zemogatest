﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTest.WebApi.Models
{
    public class AddComment
    {
        public string BlogId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
    }
}
