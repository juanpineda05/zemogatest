﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZemogaTest.DataAccess.DTO
{
    public class CommentsDTO
    {
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime PublishAt { get; set; }
    }
}
