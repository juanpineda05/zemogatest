﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZemogaTest.DataAccess.DTO
{
    public class BlogDTO
    {
        public string id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public string State { get; set; }
        public DateTime? ApprovalAt { get; set; }
        public DateTime PublishAt { get; set; }
        public List<CommentsDTO> Comments { get; set; }

    }
}
