﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZemogaTest.DataAccess.DTO;

namespace ZemogaTest.DataAccess.DataAccess.Interfaces
{
    public interface IUserRepository
    {
        Task<UserDTO> GetUserById(string id);

        Task<UserDTO> ValidateUser(string userName, string password);
    }
}
