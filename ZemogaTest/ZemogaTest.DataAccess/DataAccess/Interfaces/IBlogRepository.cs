﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZemogaTest.DataAccess.CosmosDB;
using ZemogaTest.DataAccess.DTO;

namespace ZemogaTest.DataAccess.DataAccess.Interfaces
{
    public interface IBlogRepository
    {
        Task Create(BlogDTO data);

        Task Update(string id, BlogDTO data);

        Task Delete(string id);

        Task<IEnumerable<BlogDTO>> GetPostPending();

        Task ChangeStatePost(string id, string state);

        Task<IEnumerable<BlogDTO>> Get(Expression<Func<BlogDTO, bool>> predicate);

        Task CreateComment(string blogId, CommentsDTO data);
    }
}
