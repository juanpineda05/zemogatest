﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZemogaTest.DataAccess.CosmosDB;
using ZemogaTest.DataAccess.DataAccess.Interfaces;
using ZemogaTest.DataAccess.DTO;

namespace ZemogaTest.DataAccess.DataAccess.Implementation
{
    public class BlogRepository : IBlogRepository
    {
        private readonly IDocumentRepository<BlogDTO> _blogPostRepository;

        private const string PENDINGAPRROVAL = "Pending publish approval";
        private const string APPROVE = "Approve";
        private const string REJECT = "Reject";

        public BlogRepository()
        {
            _blogPostRepository = new DocumentRepository<BlogDTO>("Zemoga", "BlogPost");
        }

        public Task ChangeStatePost(string id, string state)
        {
            BlogDTO _data = _blogPostRepository.GetItemAsync(id).Result;
            _data.State = state;
            _data.ApprovalAt = (state == APPROVE || state == REJECT) ? DateTime.UtcNow : (DateTime?)null;
            return _blogPostRepository.UpdateItemAsync(id, _data);
        }

        public Task Create(BlogDTO data)
        {
            data.PublishAt = DateTime.UtcNow;
            data.Comments = new List<CommentsDTO>();
            data.State = PENDINGAPRROVAL;
            return _blogPostRepository.CreateItemAsync(data);
        }

        public Task CreateComment(string blogId, CommentsDTO data)
        {
            BlogDTO _result = _blogPostRepository.GetItemAsync(blogId).Result;
            if (_result.State == PENDINGAPRROVAL || _result.State == REJECT)
            {
                throw new Exception("Blog can´t add comments");
            }
            data.PublishAt = DateTime.UtcNow;
            _result.Comments.Add(data);
            _result.Comments = _result.Comments;
            return _blogPostRepository.UpdateItemAsync(blogId, _result);
        }

        public Task Delete(string id)
        {
            return _blogPostRepository.DeleteItemAsync(id);
        }

        public Task<IEnumerable<BlogDTO>> Get(Expression<Func<BlogDTO, bool>> predicate)
        {
            return _blogPostRepository.GetItemsAsync(predicate);
        }

        public Task<IEnumerable<BlogDTO>> GetPostPending()
        {
            throw new NotImplementedException();
        }

        public Task Update(string id, BlogDTO data)
        {
            BlogDTO _result = _blogPostRepository.GetItemAsync(id).Result;
            if (_result.State == PENDINGAPRROVAL)
            {
                throw new Exception("Blog can´t be edited");
            }
            _result.Text = string.IsNullOrEmpty(data.Text) ? _result.Text : data.Text;
            _result.Author = string.IsNullOrEmpty(data.Author) ? _result.Author : data.Author;
            return _blogPostRepository.UpdateItemAsync(id, _result);
        }
    }
}
