﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZemogaTest.DataAccess.CosmosDB;
using ZemogaTest.DataAccess.DataAccess.Interfaces;
using ZemogaTest.DataAccess.DTO;

namespace ZemogaTest.DataAccess.DataAccess.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly IDocumentRepository<UserDTO> _userRepository;

        public UserRepository()
        {
            _userRepository = new DocumentRepository<UserDTO>("Zemoga", "Users");
        }

        public Task<UserDTO> GetUserById(string id)
        {
            return _userRepository.GetItemAsync(id);
        }

        public Task<UserDTO> ValidateUser(string userName, string password)
        {
            return _userRepository.GetItemAsync(c => c.UserName == userName && c.Password == password);
        }
    }
}
